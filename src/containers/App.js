// app/index.js

import React, {Component} from 'react';
import {Text, StyleSheet,Alert, View} from 'react-native';
import {Router, Scene,ActionConst} from 'react-native-router-flux';

import Authentication from '../components/login/Authentication'
import HomePage from '../components/home/Homepage'


import { connect } from 'react-redux'
import { fetchData } from '../actions/actions'
import { Provider } from 'react-redux'

import configureStore from '../store/configureStore'
 const store = configureStore()


// const ReduxApp = () => (
//   <Provider store={store}>
//     <App />
//   </Provider>
// )



const App = (props) => {
  const {
    container,
    text,
    button,
    buttonText
  } = styles

  return (
   <Provider store={store}>
   <Router>
        
        <Scene key='root'>
          <Scene
            component={Authentication}
            hideNavBar={true}
            initial={true}
            key='Authentication'
            title='Authentication'
            
          />
          <Scene
            component={HomePage}
            hideNavBar={true}
            key='HomePage'
            title='Home Page'
            type={ActionConst.RESET}

          />
        </Scene>

      </Router>
      </Provider>
  )
}

styles = StyleSheet.create({
  container: {
    marginTop: 100
  },
  text: {
    textAlign: 'center'
  },
  button: {
    height: 60,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0b7eff'
  },
  buttonText: {
    color: 'white'
  }
})

// function mapStateToProps (state) {
//   return {

//     userDatarr: state.userData
//   }
// }

// function mapDispatchToProps (dispatch) {
//   return {
//     fetchData: () => dispatch(fetchData())
//   }
// }

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(App)

export default App