import React, {Component} from 'react';
import {TouchableHighlight,Alert, AsyncStorage,StyleSheet,Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import { Field,reduxForm } from 'redux-form'
import { connect } from 'react-redux'

import { login } from '../../actions/user.js'


// const submit = values => {
//    authenticate()
//    //login(values.email, values.password);
// }

const renderInput = ({ input: { onChange, ...restInput }}) => {
  return <TextInput style={styles.input} onChangeText={onChange} {...restInput} />
}

const Authentication = (props) => {
 const { handleSubmit } = props
 const {login} = props

 const {
    container,
    text,
    button,
    buttonText,
    mainContent
  } = styles
  
    return (
       <View style={styles.container}>
          {props.userData.isFetching && <Text> LOGGING IN </Text>}
          {props.userData.dataFetched ? Actions.HomePage(): null}
          {props.userData.error && <Text> {props.userData.error} </Text>}
         <Text>Email:</Text>
         <Field name="email" component={renderInput} />
         
         <Text>Password:</Text>
         <Field  name="password" component={renderInput} />

      <TouchableOpacity onPress={handleSubmit(login)}>
        <Text style={styles.button}>Submit</Text>
      </TouchableOpacity>


      </View>
    );
  
}

var styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 10,
    padding: 20,
    marginBottom: 40,
    backgroundColor: '#ffffff',
    flex: 1,
    
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 90,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
});

function mapStateToProps (state) {
  return {
    userData: state.userData
  }
}

function mapDispatchToProps (dispatch) {
  return {
    login: (val) => dispatch(login(val))
  }
}



Authentication = reduxForm({
  form: 'loginForm'  // a unique identifier for this form
})(Authentication)

// You have to connect() to any reducers that you wish to connect to yourself
const AuthenticationComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Authentication)

export default AuthenticationComponent