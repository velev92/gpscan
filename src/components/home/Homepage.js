import React, {Component} from 'react';
import {Alert, Image, TouchableOpacity, AppRegistry,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  AlertIOS} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Camera from 'react-native-camera';
import PoisonGrizach from '../forms/form1';
import NonPoisonGrizach from '../forms/form2';
import Lamp from '../forms/form3';
import { connect } from 'react-redux'

import { logout } from '../../actions/user.js'

class HomePage extends Component {

 constructor(props) {
    super(props);
    
    this.state = {
      deviceID: null,
      error: null,
      device: null,
      token: null,
      torchMode: 'off',
      cameraType: 'back',
      showCamera: true,
      cameraType: Camera.constants.Type.back
    };
    

    this._onBarCodeRead = this._onBarCodeRead.bind(this);
    this._fetchDevice = this._fetchDevice.bind(this);
    this.segue = this.segue.bind(this);
}

segue(response)
 {
    
    

    if(response.status == 200)
    {
        this.setState({
           error: <Text style = {{fontSize: 18, color: 'red'}} > Изпратено успешно  </Text>
        })
    }
    else
    {
        this.setState({
           error: <Text style = {{fontSize: 18, color: 'red'}} > Изпращането е неуспешно </Text>
        })
    }


    this.setState({
      device: null,
      deviceID: null,
      showCamera: true,
      form: null,
    })
 }

_onBarCodeRead(e) {
        
        
        
        this.setState({showCamera: false,
                        deviceID: e.data });
       
        this._fetchDevice(this.state.deviceID)
       
      
        
 }

_fetchDevice(deviceID)
 {
    //Alert.alert(this.props.userData.token);
    
    fetch("https://afternoon-earth-36025.herokuapp.com/devicecrud/device2/" + this.state.deviceID
        , {
        method: "GET", 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      })
      .then((response) => response.json())
      .then((responseData) => {

        

          if(responseData === null)
          {
               this.setState({showCamera: true, error: <Text style = {{fontSize: 18, color: 'red'}} > {deviceID} не е регистриранo </Text> });
          }
          else
          {
              this.setState({device: responseData, error: null});
          }

          
        
      }).done();
 }
 

  render() {
        var camera = <Camera
                      ref="cam"
                      style={styles.container}
                      onBarCodeRead={this._onBarCodeRead}
                      type={this.state.cameraType}>
                 </Camera>

   var form = null;
    
   if (this.state.device) 
   {
          if(this.state.device.deviceType === 'type1')
          {
              form = <PoisonGrizach id = {this.state.deviceID} onFinish = {this.segue}/>
          }
          else if(this.state.device.deviceType === 'type2')
          { 
              form = <NonPoisonGrizach id = {this.state.deviceID} onFinish = {this.segue}/>
          }
          else if(this.state.device.deviceType === 'type3')
          { 
              form = <Lamp id = {this.state.deviceID} onFinish = {this.segue}/>
          }
    }
//{props.userData. ? Actions.HomePage(): null}
    return (
      <View style={styles.container}>
             {this.state.showCamera? camera : null}
             {this.state.device ? form : null}
             {this.state.error}
             {this.props.userData.token ?  null: Actions.Authentication({type: "reset"})}
             <TouchableOpacity onPress={this.props.logout}>
             <Text style={styles.button}>LOGOUT</Text>
             </TouchableOpacity>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 10,
    padding: 20,
    marginBottom: 40,
    backgroundColor: '#ffffff',
    flex: 1,
    
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 90,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
});

function mapStateToProps (state) {
  return {
    userData: state.userData
  }
}

function mapDispatchToProps (dispatch) {
  return {
    logout: () => dispatch(logout())
  }
}


const Home = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage)

export default Home