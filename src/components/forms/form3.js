import React from 'react';
var ReactNative = require('react-native');
var t = require('tcomb-form-native');
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  AlertIOS,
  Picker,
} = ReactNative;

var Form = t.form.Form;

var radio_props = [
  {label: '', value: 0 },
  {label: '', value: 1 },
  {label: '', value: 2 },
  {label: '', value: 3 },
  {label: '', value: 4 },
  {label: '', value: 5 },
  {label: '', value: 6 },
  {label: '', value: 7 },
  {label: '', value: 8 },


];

var props = [
  {label: '', value: 0 },
  {label: '', value: 20 },
  {label: '', value: 40 },
  {label: '', value: 60 },
  {label: '', value: 80 },
  {label: '', value: 100 },

];

var radio_props2 = [
  {label: 'NO', value: 0 },
  {label: 'YES', value: 1 },
];




export default class Lamp extends React.Component {
  
constructor(props) {
    super(props);

    this.state = {
     
        flies: 0,
        quantity: 0,
        additional: 0,   
    }
  
}

  onPress() {

      var more;
      if(this.state.additional == 0)
      {
          more = 'no';
      }
      else
      {
        more = 'yes';
      }

      console.log("HERE WE ARE" + this.state.flies + "      " +this.state.quantity + this.props.id)

      fetch("https://afternoon-earth-36025.herokuapp.com/eventcrud/event/", {
        method: "POST", 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          data: this.state.flies + ',' + this.state.quantity + ',' + more,
          deviceID: this.props.id
        })
      }) .then((response) => { this.props.onFinish(response) })

  }

  render() {
    return (

      <View style={styles.container}>
          <View style={styles.form}> 
            <Text style = {{fontSize: 18}} > Процент мухи </Text>
            <RadioForm

                radio_props={props}
                initial={this.state.flies}
                formHorizontal={true}
                labelHorizontal={true}
                labelWrapStyle={{marginLeft: 10}}
                onPress={(value) => {this.setState({flies:value})}}
              />
              <Text>  0%        20%     40%   60%   80%   100% </Text>
            </View>



          <View style={styles.form}> 
            <Text style = {{fontSize: 18}} > # изгорели крушки  </Text>
            <Text> Burned out Lamps </Text>
            <RadioForm

                radio_props={radio_props}
                initial={this.state.quantity}
                formHorizontal={true}
                labelHorizontal={true}
                buttonOuterSize={20}
                buttonSize={20}
                buttonStyle={{marginLeft: 3}}
                onPress={(value) => {this.setState({quantity:value})}}
            />
             <Text>  [0 => 8]  </Text> 
            </View>
              <View style={styles.form}> 

              <Text style = {{fontSize: 18}} > Промяна на лепило </Text>
              <RadioForm
                  radio_props={radio_props2}
                  initial={this.state.additional}
                  formHorizontal={true}
                  labelHorizontal={true}
                  onPress={(value) => {this.setState({additional:value})}}
                />
                </View>
            
            <TouchableHighlight style={styles.button} onPress={this.onPress.bind(this)} underlayColor='#99d9f4'>
              <Text style={styles.buttonText}>Submit Form</Text>
            </TouchableHighlight>
      </View>
     
    );
  }
}


var styles = StyleSheet.create({
  container: {
    flex: 0,
    marginTop: 20,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  form:
  {
    marginBottom: 20,
    marginTop: 20,
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 100,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

