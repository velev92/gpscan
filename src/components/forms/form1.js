import React from 'react';
var ReactNative = require('react-native');
var t = require('tcomb-form-native');
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  AlertIOS,
  Picker,
} = ReactNative;

var Form = t.form.Form;

var radio_props = [
  {label: '', value: 100 },
  {label: '', value: 80 },
  {label: '', value: 60 },
  {label: '', value: 40 },
  {label: '', value: 20 },
  {label: '', value: 0 },

];

var radio_props2 = [
  {label: 'NO', value: 0 },
  {label: 'YES', value: 1 },
];




export default class PoisonGrizach extends React.Component {
  
constructor(props) {
    super(props);

    this.state = {
     
        quantity: 100,
        additional: 0,   
    }
  
}

  onPress() {

      var more;
      if(this.state.additional == 0)
      {
          more = 'no';
      }
      else
      {
        more = 'yes';
      }

      console.log("HERE WE ARE" + this.state.quantity + "      " +this.state.additional + this.props.id)

      fetch("https://afternoon-earth-36025.herokuapp.com/eventcrud/event/", {
        method: "POST", 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          data: this.state.quantity + ',' + more,
          deviceID: this.props.id
        })
      }) .then((response) => { this.props.onFinish(response) })

  }

  render() {
    return (

      <View style={styles.container}>
          <View style={styles.form}> 
            <Text style = {{fontSize: 18}}> Сума на отрова остава </Text>
            <RadioForm

                radio_props={radio_props}
                initial={this.state.quantity1}
                formHorizontal={true}
                labelHorizontal={true}
                labelWrapStyle={{marginLeft: 10}}
                onPress={(value) => {this.setState({quantity:value})}}
              />
              <Text> 100%  80%   60%   40%   20%   0% </Text>
            </View>
              <View style={styles.form}> 
              <Text style = {{fontSize: 18}}> Замяна на отрова </Text>
              <RadioForm
                  radio_props={radio_props2}
                  initial={this.state.additional}
                  formHorizontal={true}
                  labelHorizontal={true}
                  onPress={(value) => {this.setState({additional:value})}}
                />
                </View>
            
            <TouchableHighlight style={styles.button} onPress={this.onPress.bind(this)} underlayColor='#99d9f4'>
              <Text style={styles.buttonText}>Submit Form</Text>
            </TouchableHighlight>
      </View>
     
    );
  }
}


var styles = StyleSheet.create({
  container: {
    marginTop: 20,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  form:
  {
    marginBottom: 40,
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 100,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

