import { AUTHENTICATING_USER, AUTHENTICATING_USER_SUCCESS, AUTHENTICATING_USER_FAILURE,DEAUTHENTICATE } from '../constants/constants'

export function authenticateUser() {
  return {
    type: AUTHENTICATING_USER,
    
  }
}

export function authenticateUserSuccess(data) {
  return {
    type: AUTHENTICATING_USER_SUCCESS,
    data,
  }
}

export function authenticateUserFailure(data) {
  return {
    type: AUTHENTICATING_USER_FAILURE,
    data: data.message
  }
}

export function authenticate(val) {
  return (dispatch) => {
    dispatch(authenticateUser())
  }
}

export function deAuth() {
  return {
    type: DEAUTHENTICATE,
  }
}