

import { authenticate } from './actions.js'
import {authenticateUserSuccess,authenticateUserFailure,deAuth} from './actions.js'
export function login(value) {
  return (dispatch) => {
    //console.log('YOOO')
    console.log(JSON.stringify({
          email: value.email, 
          password: value.password, 
        }));
    
    // We use this to update the store state of `isLoggingIn`          
    // which can be used to display an activity indicator on the login
    // view.
    
    dispatch(authenticate(value))


     fetch("https://afternoon-earth-36025.herokuapp.com/auth/login", {
        method: "POST", 
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: value.email, 
          password: value.password, 
        })
      })
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.success)
        {
          dispatch(authenticateUserSuccess(responseData))
        }
        else
        {
          dispatch(authenticateUserFailure(responseData))
        }
       
        
        console.log(responseData)
        
      }).done();

      





    // Note: This base64 encode method only works in NodeJS, so use an
    // implementation that works for your platform:
    // `base64-js` for React Native,
    // `btoa()` for browsers, etc...
    // const hash = new Buffer(`${username}:${password}`).toString('base64')
    // return fetch('https://httpbin.org/basic-auth/admin/secret', {
    //   headers: {
    //     'Authorization': `Basic ${hash}`
    //   }
    // })
    // .then(response => response.json().then(json => ({ json, response })))
    // .then(({json, response}) => {
    //   if (response.ok === false) {
    //     return Promise.reject(json)
    //   }
    //   return json
    // })
    // .then(
    //   data => {
    //     // data = { authenticated: true, user: 'admin' }
    //     // We pass the `authentication hash` down to the reducer so that it
    //     // can be used in subsequent API requests.

    //     //dispatch(loginSuccess(hash, data.user))
    //   },
    //   (data) => dispatch(loginFailure(data.error || 'Log in failed'))
    // )
  }
}

export function logout() 
{
      return (dispatch) => {
          dispatch(deAuth())
      }
}