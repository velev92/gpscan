import { combineReducers } from 'redux'
import userData from './userReducer'
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
    userData,
    form: formReducer
})

export default rootReducer