import { AUTHENTICATING_USER, AUTHENTICATING_USER_SUCCESS, AUTHENTICATING_USER_FAILURE , DEAUTHENTICATE} from '../constants/constants'


		const initialState = 
		{
		  userData: null,
			token: null,
		  dataFetched: false,
		  isFetching: false,
		  error: false

		}

		export default function userReducer(state = initialState, action)
		{
			switch(action.type)
			{
					case AUTHENTICATING_USER:
				      return {
				        ...state,
				        isFetching: true,
								
				      }
				    case AUTHENTICATING_USER_SUCCESS:
				      return {
				        ...state,
				        isFetching: false,
								dataFetched: true,
				        userData: action.data.user,
								token: action.data.token
				      }
				    case AUTHENTICATING_USER_FAILURE:
				      return {
				        ...state,
				        isFetching: false,
				        error: action.data
				      }
						case DEAUTHENTICATE:
							return{
								...state,
								userData: null,
								token: null,
		  					dataFetched: false,
		  					isFetching: false,
		  					error: false
							}
				    default:
				      return state
			}
		}