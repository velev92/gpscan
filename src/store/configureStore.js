import { createStore ,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'remote-redux-devtools';
import app from '../reducers'

const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000, host: 'localhost' });

export default function configureStore() {
  let store = createStore(app,composeEnhancers(applyMiddleware(thunk)));
  return store
}