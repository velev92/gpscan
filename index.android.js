import React from 'react'
import {AppRegistry} from 'react-native';

import { Provider } from 'react-redux'
import configureStore from './src/store/configureStore'


import App from './src/containers/App';



AppRegistry.registerComponent('GPScan', () => App);
